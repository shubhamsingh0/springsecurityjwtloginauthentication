package com.microservices.service1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.service1.model.LoginModel;
import com.microservices.service1.security.AuthenticationDetail;
import com.microservices.service1.security.JwtUtils;

@RestController
public class TryController {

	@Autowired
	private AuthenticationManager authManager;
	
	@Autowired
	private AuthenticationDetail authDetail;
	
	@Autowired
	private JwtUtils jwtUtil;
	
	@GetMapping("/getResponse")
	public String getResponse() {
		return "Hello";
	}
	
	@PostMapping("/authenticate")
	public ResponseEntity<String> login(@RequestBody(required = true) LoginModel loginModel) {
		
		try {
			
		Authentication authentication = new UsernamePasswordAuthenticationToken(loginModel.getUserName(),loginModel.getPassword());
		authManager.authenticate(authentication);
		
		}catch (Exception e) {
			return ResponseEntity.ok(e.getMessage().toString());
		}
		
		UserDetails user = authDetail.loadUserByUsername(loginModel.getUserName());
		String generatedToken = jwtUtil.generateToken(user);
		
		return ResponseEntity.ok(generatedToken);
	}
	
	
}
