package com.microservices.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityBasicLoginAuthentication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityBasicLoginAuthentication.class, args);
	}

}
