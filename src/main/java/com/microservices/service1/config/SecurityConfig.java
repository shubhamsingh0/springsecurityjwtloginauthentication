package com.microservices.service1.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.microservices.service1.security.AuthenticationDetail;
import com.microservices.service1.security.JwtFilters;

/*	Step 0 -> Create CustomUserDetailsSerivceClass by implementing UserDetailsService and overriding its loadUserByUsername(String username)
 	Step 1 -> Create SecurityConfig by extending WebSecurityConfigurerAdapter 
	Step 2 -> Override configure(AuthenticationManagerBuilder auth)
	Step 3 -> Create AuthenticatioinManagerBean as default will not work using super.authenticationManagerBean();
	Step 4 -> Create PasswordEncode to encode the password and for internal verification, just pass the plain password
			  ApplicationManager will verify the plain text passcode with Bcrypted Hash
	Step 5 -> Create Authentication authentication = new UsernamePasswordAuthenticationToken(UserName,Password) in controller;
			  and call authManager.authenticate(authentication) of AuthenticationManager
	Step 6 -> Add JwtUtils
	Step 7 -> If there is not any exception after authentication in TryController then generate Token
*/
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private AuthenticationDetail authenticationDetail;
	
	@Autowired
	private JwtFilters jwtFilter;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(authenticationDetail).passwordEncoder(passwordEncoder());
		
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.httpBasic().disable().csrf().disable()
		.cors().disable().authorizeRequests().antMatchers("/authenticate")
		.permitAll().anyRequest().authenticated().and().exceptionHandling().and().sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS).
		and().addFilterBefore(jwtFilter,UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return NoOpPasswordEncoder.getInstance();
//	}
}
