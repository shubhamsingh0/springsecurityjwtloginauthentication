package com.microservices.service1.security;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.microservices.service1.model.LoginModel;

@Service
public class AuthenticationDetail implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		LoginModel auth = create();
		
			if(auth.getUserName().equals(username)) {
				return new User(auth.getUserName(),auth.getPassword(),new ArrayList<>());
			}else{
				throw new UsernameNotFoundException("No userName found");
			}
	}
	private LoginModel create(){
		return new LoginModel("root","$2a$10$uobgULBu0I6RqHMxyhWbgus4.rDPtnWMoItsLTSF85tTWiuj0/qym");
	}
	
}
